English|[中文](README.md)

# sample

#### Introduction

Ascend sample，Please enter the corresponding folder to get the application according to your needs, or click the description link below to select the application you need.


#### 链接地址

<details open><summary>common：Third-party dependencies and environmental installation guide document folder。</summary><blockquote>

- [install_opencv](https://gitee.com/ascend/samples/tree/master/common/install_opencv)：The installation Notes of opencv.
- [install_presenteragent](https://gitee.com/ascend/samples/tree/master/common/install_presenteragent)：The installation Notes of presenteragent.
- [install_python3env](https://gitee.com/ascend/samples/tree/master/common/install_python3env)：The installation Notes of python3 environment.
</blockquote></details> 

<details open><summary>classification:A classification application based on googlenet, the input is a picture, and the output is a picture.</summary><blockquote>

- [for_atlas200dk_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/classification/for_atlas200dk_1.7x.0.0_c++):This branch is a C++ sample based on version 1.7x.0.0 running on 200DK. Use opencv to preprocess the image, classify the objects in the preprocessed image, and finally perform the corresponding post-processing through opencv.  
- [for_atlas200dk_1.7x.0.0_python](https://gitee.com/ascend/samples/tree/master/classification/for_atlas200dk_1.7x.0.0_python):This branch is a python sample running on 200DK based on version 1.7x.0.0. Use dvpp to preprocess the image, classify the objects in the preprocessed image, and finally perform corresponding post-processing through pillow.   
- [for_atlas300_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/classification/for_atlas300_1.7x.0.0_c++):This branch is a C++ sample based on version 1.7x.0.0 running on the ai1 environment. Use opencv to preprocess the image, classify the objects in the preprocessed image, and finally perform the corresponding post-processing through opencv. 
</blockquote></details>  

<details open><summary>classification_dynamicbatch
:A classification application based on googlenet, the input is a picture, and the output is a picture.</summary><blockquote>

- [for_atlas200dk_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/classification_dynamicbatch/for_atlas200dk_1.7x.0.0_c++):This branch is a C++ sample based on version 1.7x.0.0 running on 200DK. Use opencv to preprocess the image, classify the objects in the preprocessed image, and finally perform the corresponding post-processing through opencv.  
- [for_atlas200dk_1.7x.0.0_python](https://gitee.com/ascend/samples/tree/master/classification/for_atlas200dk_1.7x.0.0_python):This branch is a python sample running on 200DK based on version 1.7x.0.0. Use dvpp to preprocess the image, classify the objects in the preprocessed image, and finally perform corresponding post-processing through pillow.   
- [for_atlas300_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/classification_dynamicbatch/for_atlas300_1.7x.0.0_c++):This branch is a C++ sample based on version 1.7x.0.0 running on the ai1 environment. Use opencv to preprocess the image, classify the objects in the preprocessed image, and finally perform the corresponding post-processing through opencv. 
</blockquote></details>  

<details open><summary>classification_multibatch
:Based on googlenet classification application, the input is video and the output is video.</summary><blockquote>

- [for_atlas200dk_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/classification_multibatch%20%20%20%20/for_atlas200dk_1.7x.0.0_c++):This branch is a C++ sample based on version 1.7x.0.0 running on 200DK. Use opencv to preprocess the video frame, classify the objects in the preprocessed video frame, and finally perform the corresponding post-processing through opencv.   
- [for_atlas300_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/classification_multibatch%20%20%20%20/for_atlas300_1.7x.0.0_c++):This branch is a C++ sample based on version 1.7x.0.0 running on the ai1 environment. Use opencv to preprocess the video frame, classify the objects in the preprocessed video frame, and finally perform the corresponding post-processing through opencv.   
</blockquote></details>
<details open><summary>classification_video:Based on googlenet classification application, the input is video and the output is video.</summary><blockquote>

- [for_atlas200dk_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/classification_video/for_atlas200dk_1.7x.0.0_c++):This branch is a C++ sample based on version 1.7x.0.0 running on 200DK. Use opencv to preprocess the video frame, classify the objects in the preprocessed video frame, and finally perform the corresponding post-processing through opencv.   
- [for_atlas300_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/classification_video/for_atlas300_1.7x.0.0_c++):This branch is a C++ sample based on version 1.7x.0.0 running on the ai1 environment. Use opencv to preprocess the video frame, classify the objects in the preprocessed video frame, and finally perform the corresponding post-processing through opencv.   
</blockquote></details>


<details open><summary>colorization:A black and white image coloring application based on alexnet, the input is a picture, and the output is a picture.</summary><blockquote>

- [for_atlas200dk_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/colorization/for_atlas200dk_1.7x.0.0_c++):This branch is a C++ sample based on version 1.7x.0.0 running on 200DK. Use opencv to preprocess the image, perform color channel prediction on the preprocessed image, and finally perform the corresponding post-processing through opencv.  
- [for_atlas200dk_1.7x.0.0_python](https://gitee.com/ascend/samples/tree/master/colorization/for_atlas200dk_1.7x.0.0_python):This branch is a python sample running on 200DK based on version 1.7x.0.0. Use opencv to preprocess the image, perform color channel prediction on the preprocessed image, and finally perform the corresponding post-processing through opencv.
- [for_atlas300_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/colorization/for_atlas300_1.7x.0.0_c++):This branch is a C++ sample based on version 1.7x.0.0 running on the ai1 environment. Use opencv to preprocess the image, perform color channel prediction on the preprocessed image, and finally perform the corresponding post-processing through opencv.
  
</blockquote></details>  

   
<details open><summary>colorization_video:Black and white image coloring application based on alexnet, the input is video, and the output is video.</summary><blockquote>

- [for_atlas200dk_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/colorization_video/for_atlas200dk_1.7x.0.0_c++):This branch is a C++ sample based on version 1.7x.0.0 running on 200DK. Use opencv to preprocess the video frame, perform color channel prediction on the preprocessed video frame, and finally perform the corresponding post-processing through opencv.  
- [for_atlas300_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/colorization_video/for_atlas300_1.7x.0.0_c++):This branch is a C++ sample based on version 1.7x.0.0 running on the ai1 environment. Use opencv to preprocess the video frame, perform color channel prediction on the preprocessed video frame, and finally perform the corresponding post-processing through opencv.
</blockquote></details>


<details open><summary>objectdetection:Target detection application based on yolov3, the input is a picture, and the output is a picture.</summary><blockquote>

- [for_atlas200dk_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/objectdetection/for_atlas200dk_1.7x.0.0_c++):This branch is a C++ sample based on version 1.7x.0.0 running on 200DK. Use opencv to preprocess the image, perform target detection on the objects in the preprocessed image, and finally perform corresponding post-processing through opencv.  
- [for_atlas200dk_1.7x.0.0_python](https://gitee.com/ascend/samples/tree/master/objectdetection/for_atlas200dk_1.7x.0.0_python):This branch is a python sample running on 200DK based on version 1.7x.0.0. Use dvpp to preprocess the image, perform target detection on the objects in the preprocessed image, and finally perform corresponding post-processing through pillow.   
- [for_atlas300_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/objectdetection/for_atlas300_1.7x.0.0_c++):This branch is a C++ sample based on version 1.7x.0.0 running on the ai1 environment. Use opencv to preprocess the image, perform target detection on the objects in the preprocessed image, and finally perform corresponding post-processing through opencv.  
</blockquote></details>     
     
<details open><summary>objectdetection_dynamic_aipp
:Target detection application based on yolov3, the input is a picture, and the output is a picture.</summary><blockquote>

- [for_atlas200dk_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/objectdetection_dynamic_aipp/for_atlas200dk_1.7x.0.0_c++):This branch is a C++ sample based on version 1.7x.0.0 running on 200DK. Use opencv to preprocess the image, perform target detection on the objects in the preprocessed image, and finally perform corresponding post-processing through opencv.  
- [for_atlas200dk_1.7x.0.0_python](https://gitee.com/ascend/samples/tree/master/objectdetection/for_atlas200dk_1.7x.0.0_python):This branch is a python sample running on 200DK based on version 1.7x.0.0. Use dvpp to preprocess the image, perform target detection on the objects in the preprocessed image, and finally perform corresponding post-processing through pillow.   
- [for_atlas300_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/objectdetection_dynamic_aipp/for_atlas300_1.7x.0.0_c++):This branch is a C++ sample based on version 1.7x.0.0 running on the ai1 environment. Use opencv to preprocess the image, perform target detection on the objects in the preprocessed image, and finally perform corresponding post-processing through opencv.  
</blockquote></details>



<details open><summary>objectdetection_video:Target detection application based on yolov3, the input is video, and the output is video.</summary><blockquote>

- [for_atlas200dk_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/objectdetection_video/for_atlas200dk_1.7x.0.0_c++):This branch is a C++ sample based on version 1.7x.0.0 running on 200DK. Use opencv to preprocess the video frame, perform target detection on the objects in the preprocessed video frame, and finally perform the corresponding post-processing through opencv.    
- [for_atlas300_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/objectdetection_video/for_atlas300_1.7x.0.0_c++):This branch is a C++ sample based on version 1.7x.0.0 running on the ai1 environment. Use opencv to preprocess the video frame, perform target detection on the objects in the preprocessed video frame, and finally perform the corresponding post-processing through opencv.  
</blockquote></details>

<details open><summary>facedetection:A face detection application based on caffe-ssd, the input is a raspberry pie camera, and the output is video.</summary><blockquote>

- [for_atlas200dk_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/facedetection/for_atlas200dk_1.7x.0.0_c++):This branch is a C++ sample based on version 1.7x.0.0 running on 200DK. Use dvpp to preprocess video frames, perform face detection on the preprocessed video frames, and finally perform corresponding post-processing.  
- [for_atlas200dk_1.7x.0.0_python](https://gitee.com/ascend/samples/tree/master/facedetection/for_atlas200dk_1.7x.0.0_python):This branch is a python sample running on 200DK based on version 1.7x.0.0. Use dvpp to preprocess video frames, perform face detection on the preprocessed video frames, and finally perform corresponding post-processing. 
</blockquote></details> 

<details open><summary>mark_detection:Mask recognition application based on yolov3, the input is a picture, and the output is a picture.</summary><blockquote>

- [for_atlas200dk_1.7x.0.0_python](https://gitee.com/ascend/samples/tree/master/mark_detection/%20for_atlas200dk_1.7x.0.0_python):This branch is a python sample running on 200DK based on version 1.7x.0.0. Use opencv and pillow to preprocess the image, perform mask recognition on the preprocessed image, and finally perform corresponding post-processing through opencv and pillow. 
- [for_atlas200dk_1.3x.0.0_python](https://gitee.com/ascend/samples/tree/master/mark_detection/for_atlas200dk_1.3x.0.0_python):This branch is a python sample based on version 1.3x.0.0 running on 200DK. Use opencv to preprocess the image, perform mask recognition on the preprocessed image, and finally perform the corresponding post-processing through opencv. 
</blockquote></details>


<details open><summary>mark_detection_video:Mask recognition application based on yolov3, the input is video, and the output is video.</summary><blockquote>

- [for_atlas200dk_1.3x.0.0_c++](https://gitee.com/ascend/samples/tree/master/mark_detection_video/for_atlas200dk_1.3x.0.0_c++):This branch is based on the 1.3x.0.0 version of C++ samples running on 200DK. Use dvpp to preprocess the video frame, perform mask recognition on the preprocessed video frame, and finally perform corresponding post-processing. 
- [for_atlas200dk_1.3x.0.0_python](https://gitee.com/ascend/samples/tree/master/mark_detection_video/for_atlas200dk_1.3x.0.0_python):This branch is a python sample based on version 1.3x.0.0 running on 200DK. Use opencv to preprocess the video frame, perform mask recognition on the preprocessed video frame, and finally perform the corresponding post-processing.
</blockquote></details>

