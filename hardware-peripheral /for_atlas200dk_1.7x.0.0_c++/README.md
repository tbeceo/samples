# HardwareExpansion-Atlas200DK

#### 介绍
本仓包含Atlas200DK各硬件接口的使用样例，各文件夹对应不同硬件设备的样例，以供用户参考。具体说明如下。

1. [gpio](https://gitee.com/ascend/samples/blob/master/hardware-peripheral%20/for_atlas200dk_1.7x.0.0_c++/gpio/README.md)：Atlas200DK开发者板使用7个GPIO的样例。

2. [i2c](https://gitee.com/ascend/samples/blob/master/hardware-peripheral%20/for_atlas200dk_1.7x.0.0_c++/i2c/README.md)：Atlas200DK开发者板使用I2C的样例。

4. [uart](https://gitee.com/ascend/samples/blob/master/hardware-peripheral%20/for_atlas200dk_1.7x.0.0_c++/uart/README.md)：Atlas200DK开发者板使用UART1串口收发数据的样例。


