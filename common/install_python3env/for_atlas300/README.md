中文|[English](README_EN.md)

# 安装Python3.6运行环境<a name="ZH-CN_TOPIC_0228768065"></a>

1.  安装pip3  
    **sudo apt-get install python3-pip**    
    **python3 -m pip install pip --user** 
2.  安装依赖    
    **sudo apt-get install libtiff5-dev libjpeg8-dev zlib1g-dev libfreetype6-dev liblcms2-dev libwebp-dev tcl8.6-dev tk8.6-dev python-tk**

3.  安装 python 库  
    **python3 -m pip install --upgrade pip --user**  
    **python3 -m pip install pillow --user**  
    **python3 -m pip install protobuf --user**  
    **python3 -m pip install Cython --user**  
    **python3 -m pip install numpy --user** 