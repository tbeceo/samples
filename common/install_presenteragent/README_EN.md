English|[中文](README.md)

# install_presenteragent

#### Introduction

install_presenteragent,Please select the corresponding folder according to your own development environment, or click the description link below to select the required presenteragent installation guide document.

#### 使用说明

1. [for_atlas200dk](https://gitee.com/ascend/samples/tree/master/common/install_presenteragent/for_atlas200dk)

   Atlas200dk environment installation presenteragent guide document.

2. [for_atlas300](https://gitee.com/ascend/samples/tree/master/common/install_presenteragent/for_atlas300)

   Atlas300 environment installation presenteragent guide document.